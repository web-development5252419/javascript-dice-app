'use strict';

const diceOneElement = document.querySelectorAll('.dice');
const diceTwoElement = document.querySelectorAll('.dice-second');
const diceContainerOptionalElement = document.querySelector('.dice-container-optional');
const rollDiceButton = document.querySelector('.roll-dice-button');
const rolledDiceValue = document.querySelector('.rolled-dice-value');
const diceOneOption = document.getElementById('option1');
const diceTwoOption = document.getElementById('option2');

let isTwoDice = false;

diceOneOption.checked = true;

const generateRandomDiceNumber = function () {
    return Math.trunc(Math.random() * 6 + 1);
}

const playDiceRollAnimation = function (diceElement) {
    diceElement.classList.remove('dice-animation');
    void diceElement.offsetWidth;
    diceElement.classList.add('dice-animation');
}

const displayRolledDice = function (diceElement, rolledDiceValue) {
    if (!diceElement.classList.contains('hidden')) {
        diceElement.classList.add('hidden');
    }

    if (diceElement.className.includes(rolledDiceValue.toString())) {
        diceElement.classList.remove('hidden');
    }
}

diceOneOption.addEventListener('click', function () {
    diceTwoOption.checked = false;
    isTwoDice = false;
    diceContainerOptionalElement.classList.add('hidden');
    rolledDiceValue.textContent = 'Click Roll Dice to Roll!';
});

diceTwoOption.addEventListener('click', function () {
    diceOneOption.checked = false;
    isTwoDice = true;
    diceContainerOptionalElement.classList.remove('hidden');
    rolledDiceValue.textContent = 'Click Roll Dice to Roll!';
});

rollDiceButton.addEventListener('click', function () {
    const diceOneRollValue = generateRandomDiceNumber();
    const diceTwoRollValue = generateRandomDiceNumber();
    diceOneElement.forEach(element => {
        playDiceRollAnimation(element);
        displayRolledDice(element, diceOneRollValue);
    });

    if (isTwoDice) {
        diceTwoElement.forEach(element => {
            playDiceRollAnimation(element);
            displayRolledDice(element, diceTwoRollValue);
        });
    }

    rolledDiceValue.textContent = isTwoDice ? `You Rolled a ${diceOneRollValue + diceTwoRollValue}` : `You Rolled a ${diceOneRollValue}`;
});