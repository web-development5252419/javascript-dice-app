<div align="center">
<h1>
Javascript Dice Application <br/>
https://javascript-dice-app-web-development5252419-11ad884bc31e446208c2.gitlab.io/ <br/>
</h1>
</div>
This is a small JavaScript application that showcases concepts like manipulating CSS classes, click event handling, DOM manipulation, the use of ternary operators, iteration, template literals, and reusable functions to maintain code reusability, maintainabilty and readability while adhering to the DRY principle. It works both on mobile and desktop devices.
I developed this application mainly because I need it when I play board games.
